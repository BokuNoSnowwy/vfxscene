﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateHeart : MonoBehaviour
{
    public GameObject heart;
    public float timer;
    
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= 0.2f)
        {
            heart.SetActive(true);
        }
    }
}
