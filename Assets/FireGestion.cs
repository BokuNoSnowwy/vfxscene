﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireGestion : MonoBehaviour
{
    public GameObject preShot;
    public ParticleSystem projectile;
    public SpawnProjectilesScript spawner;

    public float timer;
    public float timerBase = 0.8f;
    public bool onPreshot;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            onPreshot = true;
            preShot.gameObject.SetActive(true);
            GameObject prefabShot = Instantiate(preShot, transform.position+Vector3.right, Quaternion.identity);
            prefabShot.GetComponent<ParticleSystem>().Play();
        }

        if (onPreshot)
        {
            timer -= Time.deltaTime;
            if (timer <= 0)
            {
                GetComponentInChildren<SpawnProjectilesScript>().SpawnVFX();
                onPreshot = false;
                timer = timerBase;

            }
        }
    }
    
}
