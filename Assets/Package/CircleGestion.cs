﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleGestion : MonoBehaviour
{
    
    public List<GameObject> circleList = new List<GameObject>();

    public int passivStack = 0;

    public float timerPassiv;
    public float totalTimePassiv = 5f;

    public bool onPassiv;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            ResetStacks();
        }

        if (onPassiv)
        {
            timerPassiv -= Time.deltaTime;
            if (timerPassiv <= 0)
            {
                StopStacks();
            }
        }
    }

    public void StopStacks()
    {
        Debug.Log("Stop Stacks");
        passivStack = 0;
        foreach (var circle in circleList)
        {
            circle.GetComponent<ParticleSystem>().Stop();
            circle.SetActive(false);
        }

        onPassiv = false;
    }

    public void ResetStacks()
    {
        
        Debug.Log("Reset Stacks");
        foreach (var circle in circleList)
        {
            circle.GetComponent<ParticleSystem>().Clear();
            circle.GetComponent<ParticleSystem>().Play();
            
            if (circle.GetComponent<ParticleSystem>().isPlaying)
            {


            }
        }

        timerPassiv = totalTimePassiv;
    }
    

    public void AddStack()
    {
        
        Debug.Log("Add Stacks");
        onPassiv = true;
        
        
        if (passivStack >= circleList.Count)
        {
            ResetStacks();
        }
        else
        {
            if (!circleList[passivStack].activeSelf)
            {
                circleList[passivStack].SetActive(true);
                ResetStacks();
            }
            passivStack += 1;
        }
    }
}
