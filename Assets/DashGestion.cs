﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class DashGestion : MonoBehaviour
{
    public List<ParticleSystem> dashParts = new List<ParticleSystem>();
    public List<ParticleSystem> otherParts = new List<ParticleSystem>();

    public Vector3 pointToGo;
    public Vector3 pointToStart;


    public bool onDash;
    public float speedDash;

    public int dashOffset = 1;
// Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Dash();
            StartCoroutine(LerpPosition(pointToGo, speedDash));
            GetComponent<CircleGestion>().AddStack();
        
        }
    }

    public void Dash()
    {

        dashOffset *= -1;
        
        Vector3 pos = transform.position;
        pointToGo = transform.position + new Vector3(Random.Range(2f, 2.5f), 0, Random.Range(2f, 2.5f))*dashOffset;
        pointToStart = pos;
        
        transform.LookAt(pointToGo);
        
        /*
        Vector3 newPosRotation = Vector3.RotateTowards(transform.position, pointToGo, 100f, 0f);
        Debug.Log(newPosRotation);
        
        //transform.rotation = Quaternion.LookRotation(new Vector3(newPosRotation.y,newPosRotation.y,0f));
        //transform.rotation = Quaternion.LookRotation(newPosRotation);
        transform.rotation = new Quaternion(0f,Quaternion.LookRotation(newPosRotation).y,transform.rotation.z,transform.rotation.w);
        */
        

        
        onDash = true;
    }
    
    IEnumerator LerpPosition(Vector3 targetPosition, float duration)
    {
        float time = 0;
        Vector3 startPosition = transform.position;

        while (time < duration)
        {
            ManipulateParticles(true);
            transform.position = Vector3.Lerp(startPosition, targetPosition, time / duration);
            time += Time.deltaTime;
            yield return null;
        }
        
        ManipulateParticles(false);
        transform.position = targetPosition;
    }

    public void ManipulateParticles(bool value)
    {
        foreach (var particle in dashParts)
        {
            particle.gameObject.SetActive(value);

        }

        foreach (var particle in otherParts)
        {
            //particle.transform.rotation = transform.rotation;
            particle.gameObject.SetActive(value);

        }
    }
}
