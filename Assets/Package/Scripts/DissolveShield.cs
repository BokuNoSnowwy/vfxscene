﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissolveShield : MonoBehaviour
{
    public Material mat;
    public float timer, ratio, factor, startDissolve, maxDuration;
    // Start is called before the first frame update
    void Start()
    {
        mat.SetFloat("Vector1_8EACFFE4", 0f);
        ratio = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (timer >= startDissolve)
        {
            ratio += factor;
            mat.SetFloat("Vector1_8EACFFE4", ratio);
        }

        if (timer > maxDuration)
        {
            ratio = 0f;
            mat.SetFloat("Vector1_8EACFFE4", ratio);
            timer = 0f;
        }

        GetComponent<Renderer>().material = mat;
    }
}
